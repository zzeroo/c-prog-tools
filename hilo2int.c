/*
 * Dieses kleine Werkzeug soll beim Verstehen der HIGH/ LOW Byte Problematik helfen.
 *
 * Dem Werkzeug k�nnen zwei Byte in der Reihenfolge HIGHByte -> LOWByte �bergeben
 * werden. Das Programm gibt dann den Integer Wert der beiden Byte zur�ck.
 */

#include <stdio.h>
#include <stdlib.h>


int main( int argc, char *argv[] ) {
 
  // Check Parameters
  if (argc != 3) {
    printf("\nusage: %s HIGHByte LOWByte\n\n", argv[0]);
    exit(1); 
  }

  // Setup vars
  int HIGHByte  = atoi(argv[1]);
  int LOWByte   = atoi(argv[2]);
  int Result;

  // Here is where the magic happens
  Result = (HIGHByte * 256) + LOWByte;
  
  // Print out our Erkenntnisse
  printf("\nDie Integer Zahl wird nach folgender Formel berechnet:\n");
  printf("(HIGHByte (MSB) * 256) + LOWByte\n\n");

  printf("Der Integer Wert f�r\nHighByte:\t%d\nLOWByte:\t%d\nist %d", HIGHByte, LOWByte, Result);
}
