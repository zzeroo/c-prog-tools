/*
 * Dieses kleine Werkzeug soll beim Verstehen der HIGH/ LOW Byte Problematik helfen.
 *
 * Dem Programm wird eine Integer Zahl �bergeben und es liefert die beiden
 * HIGH und LOWByte zur�ck.
 */

#include <stdio.h>
#include <stdlib.h>


int main( int argc, char *argv[] ) {
  
  if (argc != 2) {
    printf("\nusage: %s Integer\n\n", argv[0]);
    exit(1); 
  }

  int Integer = atoi(argv[1]);
  unsigned char HIGHByte;
  unsigned char LOWByte;

  // Here is where the magic happens
  LOWByte = Integer & 0xFF;
  HIGHByte = Integer >> 8;

  printf("\nDie Zahl wird nach folgender Formel berechnet:\n");
  printf("LOWByte  = Integer & 0xFF;\n");
  printf("HIGHByte = Integer & >> 8;\n\n");

  printf("Die Zahl %d entspricht demnach\nHighByte:\t%d\nLOWByte:\t%d\n", Integer, HIGHByte, LOWByte);
}
